Сервис уведомлений

Сборка: 
    docker-compose -f build/docker-compose.yml up -d

Запуск: 
    localhost:8000

Реализовано
  Добавление, редактирование, удаление клиентов и сообщений
  Отправка сообщений на сервер 

Особенности реализации:
    При добавлении рассылки формируется очередь с указанием идентификатора клиента и даты времени для отправки
    с учетом часового пояса
    Отправка сообщений через запрос к API

Не реализовано
    Фоновая задача для автоматической отправки
    При отправке на сервер возникает ошибка 400. Причина не устранена
    



