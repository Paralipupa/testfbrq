#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR/..
pwd

echo Migrate
python manage.py migrate

echo Run server
if [ $? -eq 0 ]
then
  python manage.py runserver 0.0.0.0:8000
fi

