import grequests

import os, re
import logging
from django.db.models import Q
from django.utils import timezone
from datetime import datetime
from typing import Iterable, Mapping, Any, Dict, Tuple
from .helpers import get_ids, get_client_id
from .models import Sending, Queue
from .serializer import QueueSerializer

logger = logging.getLogger(__name__)


class SendServer:
    """Отправка сообщений на сервер"""

    def __init__(self, data: list = None) -> None:
        self.data = data

    def run(self):
        pass
        url = os.environ.get("SERVER_URL", "")
        token = os.environ.get("SERVER_JWT_TOKEN", "")
        head = {"Authorization": "Bearer {}".format(token),
                "Content-Type": "application/json"
                }
        try:
            req = [
                grequests.post(
                    f"{url}{x['message']['id']}",
                    headers=head,
                    data={
                        "id": int(x["message"]["id"]),
                        "text": x["message"]["content"],
                        "phone": x["client"]["phone"],
                        "client": x["client"]["id"],
                    },
                )
                for x in self.data
            ]
        except Exception as ex:
            pass
        append_list = list()
        for r in grequests.imap(req, size=16):
            message_id, client_id = get_ids(r.request.body)
            # client_id = get_client_id(r.request.body)
            code = r.status_code
            append_list.append(
                Sending(
                    message_id=message_id,
                    client_id=client_id,
                    create_date=datetime.now(tz=timezone.utc),
                    status=code,
                )
            )
        if append_list:
            Queue.objects.filter(message_id=message_id).delete()
            Sending.objects.bulk_create(append_list, ignore_conflicts=True)
        return
