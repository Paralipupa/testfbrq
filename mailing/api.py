import logging
from rest_framework.generics import (
    ListAPIView,
    CreateAPIView,
    RetrieveAPIView,
    UpdateAPIView,
    DestroyAPIView,
)
from rest_framework.exceptions import ValidationError
from rest_framework import status
from rest_framework.response import Response
from django.db.models import Q
from .models import *
from .serializer import *
from .helpers import get_id_from_code, replace_id, check_phone, append_to_queue
from .sending import SendServer

logger = logging.getLogger(__name__)


class ClientListView(ListAPIView, CreateAPIView):
    serializer_class = ClientSerializer

    def set_filter(self, dataSet):
        # filter
        # .....
        return dataSet.all()

    def get_queryset(self):
        return self.set_filter(Client.objects)

    def post(self, request, *args, **kwargs):
        if check_phone(request.data.get("phone", "")):
            replace_id(request.data)
            return super().post(request, *args, **kwargs)
        return Response(
            "the phone does not match the format 7XXXXXXXXXX",
            status=status.HTTP_400_BAD_REQUEST,
        )


class ClientRetrieveView(RetrieveAPIView, UpdateAPIView, DestroyAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer

    def put(self, request, *args, **kwargs):
        replace_id(request.data)
        return super().put(request, *args, **kwargs)
    
    def patch(self, request, *args, **kwargs):
        replace_id(request.data)
        return super().patch(request, *args, **kwargs)


class MessageListView(ListAPIView, CreateAPIView):
    serializer_class = SeparatorListSerializer

    def set_filter(self, dataSet):
        if self.request.GET.get("ondate"):
            ondate = self.request.GET.get("ondate")
            dataSet = dataSet.filter(
                message__from_date__lte=ondate, message__to_date__gte=ondate
            )
        if self.request.GET.get("marker"):
            marker = get_id_from_code(Marker, self.request.GET.get("marker"))
            dataSet = dataSet.filter(marker=marker)
        if self.request.GET.get("operator"):
            operator = get_id_from_code(Operator, self.request.GET.get("operator"))
            dataSet = dataSet.filter(operator=operator)
        return dataSet

    def get_queryset(self):
        return self.set_filter(Separator.objects).order_by("-id")

    def create(self, request, *args, **kwargs):
        replace_id(request.data)
        serializer = SeparatorSerializer(data=request.data)
        try:
            serializer.is_valid(raise_exception=False)
            self.perform_create(serializer)
        except AssertionError as ex:
            pass
        except ValidationError as ex:
            return Response(
                f"message already exists", status=status.HTTP_400_BAD_REQUEST
            )
        except Exception as ex:
            return Response(f"{ex}")
        if not append_to_queue(serializer.data.get("message")):
            return Response(
                f"message has not been generated (no clients found)",
                status=status.HTTP_400_BAD_REQUEST,
            )
        headers = self.get_success_headers(serializer.data)
        return Response(
            serializer.data, status=status.HTTP_201_CREATED, headers=headers
        )


class MessageRetrieveView(RetrieveAPIView, UpdateAPIView, DestroyAPIView):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer

    # def put(self, request, *args, **kwargs):
    #     replace_id(request.data)
    #     try:
    #         data = super().put(request, *args, **kwargs)
    #     except Exception as ex:
    #         pass
    #     return data
    
    def patch(self, request, *args, **kwargs):
        replace_id(request.data)
        return self.partial_update(request, *args, **kwargs)


class QueueListView(ListAPIView):
    serializer_class = QueueSerializer
    queryset = Queue.objects.all()

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(queryset, many=True)
        sending = SendServer(serializer.data)
        sending.run()
        return Response(serializer.data)


class TestServer(CreateAPIView):
    """тестовый сервер"""

    serializer_class = ServerTestSerializer

    def post(self, request, *args, **kwargs):
        serializer = ServerTestSerializer(data=request.data)
        try:
            serializer.is_valid(raise_exception=True)
        except Exception as ex:
            Response({"code": 1}, status=status.HTTP_400_BAD_REQUEST)
        return Response(
            {"code": 0}, content_type="application/json", status=status.HTTP_200_OK
        )
