from django.contrib import admin
from django.urls import path, include
from .api import ClientListView, ClientRetrieveView, MessageListView, MessageRetrieveView, QueueListView, TestServer

urlpatterns = [
    path("clients/", ClientListView.as_view() , name="api-clients"),
    path("clients/<int:pk>", ClientRetrieveView.as_view() , name="api-client"),
    path("message/", MessageListView.as_view() , name="api-messages"),
    path("message/<int:pk>", MessageRetrieveView.as_view() , name="api-message"),
    path("run/", QueueListView.as_view() , name="api-queue"),

    path("test/<int:pk>", TestServer.as_view() , name="api-test"),
]
