from django.apps import AppConfig
import sys

class MailingConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'mailing'

    def ready(self):
        if ('makemigrations' in sys.argv or 'migrate' in sys.argv):
            pass
        else:
            from gevent import monkey
            monkey.patch_all()