from rest_framework import serializers
import pytz
from django.utils import timezone
from .models import *


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = "__all__"


class MessageSerializer(serializers.ModelSerializer):
    from_date = serializers.DateTimeField(format="%Y-%m-%dT%H:%M:%S%z", default_timezone=timezone.utc)
    to_date = serializers.DateTimeField(format="%Y-%m-%dT%H:%M:%S%z", default_timezone=timezone.utc)
    class Meta:
        model = Message
        fields = "__all__"


class OperatorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Operator
        fields = "__all__"


class MarkerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Marker
        fields = "__all__"


class SendingSerializer(serializers.ModelSerializer):
    create_date = serializers.DateTimeField(format="%Y-%m-%dT%H:%M:%S%z", default_timezone=timezone.utc)
    class Meta:
        model = Sending
        fields = "__all__"

class SeparatorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Separator
        fields = "__all__"


class SeparatorListSerializer(serializers.Serializer):
    message = MessageSerializer()
    operator = OperatorSerializer()
    marker = MarkerSerializer()

    class Meta:
        model = Separator
        fields = "__all__"

class QueueSerializer(serializers.Serializer):
    message = MessageSerializer()
    client = ClientSerializer()
    from_date = serializers.DateTimeField()
    to_date = serializers.DateTimeField()

class ServerTestSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    phone = serializers.CharField()
    text = serializers.CharField()

