from django.db import models


class Message(models.Model):
    """Рассылка"""

    content = models.TextField(null=True)
    from_date = models.DateTimeField(null=True)
    to_date = models.DateTimeField(null=True)


class Operator(models.Model):
    """Коды мобильного оператора"""

    code = models.CharField(max_length=3, unique=True)
    name = models.CharField(max_length=255, null=True)


class Marker(models.Model):
    """Коды меток"""

    code = models.CharField(max_length=3, unique=True)
    name = models.CharField(max_length=255, null=True)


class Separator(models.Model):
    """Фильтр сообщений"""

    message = models.ForeignKey(Message, null=True, on_delete=models.CASCADE)
    operator = models.ForeignKey(Operator, null=True, on_delete=models.CASCADE)
    marker = models.ForeignKey(Marker, null=True, on_delete=models.CASCADE)

    class Meta:
        unique_together = ("message", "operator", "marker")


class Client(models.Model):
    """Клиенты"""

    phone = models.CharField(max_length=11, null=True, unique=True)
    operator = models.ForeignKey(Operator, null=True, on_delete=models.CASCADE)
    marker = models.ForeignKey(Marker, null=True, on_delete=models.CASCADE)
    time_zone = models.CharField(max_length=255, null=True)


class Sending(models.Model):
    """Отправленные сообщений (статистика)"""

    message = models.ForeignKey(Message, null=True, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, null=True, on_delete=models.CASCADE)
    create_date = models.DateTimeField()
    status = models.IntegerField()


class Queue(models.Model):
    """Очередь на отправку сообщений"""

    message = models.ForeignKey(Message, null=True, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, null=True, on_delete=models.CASCADE)
    from_date = models.DateTimeField(null=True)
    to_date = models.DateTimeField(null=True)

    class Meta:
        unique_together = ("message", "client")
