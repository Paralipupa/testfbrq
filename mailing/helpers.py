import re
import logging
from typing import Iterable, Mapping, Any, Dict, Tuple
from django.db.models import Q
from datetime import datetime
import pytz
from .models import *
from .serializer import *

logger = logging.getLogger(__name__)


def check_phone(phone: str) -> bool:
    return bool(re.search("^7[0-9]{10}$", phone))


def replace_id(data: dict):
    """замена кода справочника на идентификатор"""
    operator = get_id_from_code(Operator, data.pop("operator", None))
    if operator:
        data["operator"] = operator
    marker = get_id_from_code(Marker, data.pop("marker", None))
    if marker:
        data["marker"] = marker
    message = get_id_message(data)
    if message:
        data["message"] = message
    return


def get_id_from_code(model: object, code: str) -> int:
    if code:
        data = model.objects.filter(code=code)
        if len(data) == 0:
            model.objects.create(code=code)
            data = model.objects.filter(code=code)
        code = data[0].id
    return code


def get_id_message(data: dict) -> int:
    """ идентификатор сообщения по его содержимому"""
    if data.get("content") and data.get("from_date") and data.get("to_date"):
        content = data.get("content")
        from_date = data.get("from_date")
        to_date = data.get("to_date")
        data = Message.objects.filter(
            content=content,
            from_date=from_date,
            to_date=to_date,
        )
        if len(data) == 0:
            Message.objects.create(
                content=content,
                from_date=from_date,
                to_date=to_date,
            )
            data = Message.objects.filter(
                content=content,
                from_date=from_date,
                to_date=to_date,
            )
        return data[0].id
    return None


def create_filter_for_message(data_input: dict, message_id: int):
    """создать фильтр для сообщения"""
    if message_id:
        operator_id = get_id_from_code(Operator, data_input.get("operator"))
        marker_id = get_id_from_code(Marker, data_input.get("marker"))
        if operator_id and marker_id:
            Separator.objects.create(
                message_id=message_id, operator_id=operator_id, marker_id=marker_id
            )
    return


def append_to_queue(message_id: int) -> bool:
    """добавить собщения в очередь для рассылки"""
    append_list = list()
    queues = get_queues(message_id)
    for q in queues:
        append_list.append(
            Queue(
                message_id=q["mess"],
                client_id=q["client"],
                from_date=q["date1"],
                to_date=q["date2"],
            )
        )
    if append_list:
        Queue.objects.bulk_create(append_list, ignore_conflicts=True)
    return len(append_list) != 0


def is_sending_message(message_id: int, client_id: int) -> bool:
    """Проверяем отправку сообщения клиенту"""
    return bool(Sending.objects.filter(message_id=message_id, client_id=client_id, status=200))


def get_queues(message_id: int):
    """данные для рассылки"""
    data = Separator.objects.filter(message=message_id)
    dates = list(set([(x.message.from_date, x.message.to_date) for x in data]))
    markers = list(set([x.marker.id for x in data]))
    operators = list(set([x.operator.id for x in data]))
    data = Client.objects.filter(Q(operator__in=operators) | Q(marker__in=markers))
    qs = list()
    for client in data:
        if (
            is_sending_message(message_id, client.id) is False
        ):  # проверяем отправлялось ли это сообщение клиенту
            date_from = dates[0][0]
            date_to = dates[0][1]
            try:
                date_from = date_from.replace(tzinfo=pytz.timezone(client.time_zone))
                date_to = date_to.replace(tzinfo=pytz.timezone(client.time_zone))
            except Exception as ex:
                pass
            qs.append(
                {
                    "mess": message_id,
                    "client": client.id,
                    "date1": date_from,
                    "date2": date_to,
                }
            )
    return qs


def get_ids(text: str) -> Tuple[int, int]:
    try:
        message_id = int(re.findall("(?<=id=)[0-9]+", text)[0])
        client_id = int(re.findall("(?<=client=)[0-9]+", text)[0])
    except:
        return 0, 0
    return message_id, client_id

def get_client_id(text: str)->int:
    phone = re.findall("(?<=phone=)[0-9]+", text)[0]
    data = Client.objects.filter(phone=phone)
    if data:
        return data[0].id
    return 0
